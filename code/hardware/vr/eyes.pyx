# PyEPL: hardware/vr/eyes.pyx
#
# Copyright (C) 2003-2005 Michael J. Kahana
# Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
# URL: http://memory.psych.upenn.edu/programming/pyepl
#
# Distributed under the terms of the GNU Lesser General Public License
# (LGPL). See the license.txt that came with this file.

"""
This module contains the LowVEye class.
"""

import pygame

class LowVEye:
    """
    A LowVEye represents a camera view of the virtual environment.
    """
    def __init__(self, env, xsize = None, ysize = None):
        """
        Create a LowVEye of the specified dimensions.
        """
        self.env = env
        self.xsize = xsize
        self.ysize = ysize
        self.fov = 60.0
        self.x = 0
        self.y = 0
        self.z = 0
        self.yaw = 0
        self.pitch = 0
        self.roll = 0
        self.near_clipping = 0.1
        self.far_clipping = 10000.0
    def getSize(self):
        """
        Return a 2-tuple representing the dimensions of this eye's
        view.
        """
        return self.xsize, self.ysize
    def reposition(self, x, y, z, yaw, pitch, roll):
        """
        Position the camera.
        """
        self.x = x
        self.y = y
        self.z = z
        self.yaw = yaw
        self.pitch = pitch
        self.roll = roll
    def setFOV(self, fov):
        """
        Set the camera's field of view without moving it.
        """
        self.fov = fov
    def draw(self, x, y):
        """
        Render what this eye sees to the screen.
        """
        resx, resy = pygame.display.get_surface().get_size()

        self.env.render(self.x, self.y, self.z, self.yaw, self.pitch, self.roll, (self.fov * resy) / resx, resx / resy, self.near_clipping,
                        self.far_clipping, x, resy - (y + self.ysize), self.xsize, self.ysize)
