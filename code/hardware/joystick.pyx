# PyEPL: hardware/joystick.pyx
#
# Copyright (C) 2003-2005 Michael J. Kahana
# Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
# URL: http://memory.psych.upenn.edu/programming/pyepl
#
# Distributed under the terms of the GNU Lesser General Public License
# (LGPL). See the license.txt that came with this file.

"""
This module provides low-level joystick functionality.
"""

import pygame
import eventpoll

cdef object joysticks

def initialize(**options):
    """
    Do any preparation necessary before using joystick features.
    """
    global joysticks
    global zero_threshold
    if options.has_key("joystick_zero_threshold"):
        eventpoll.setJSZeroThreshold(options["joystick_zero_threshold"])
    else:
        eventpoll.setJSZeroThreshold(0.0)
    pygame.joystick.init()
    joysticks = []
    for n in xrange(pygame.joystick.get_count()):  # so that joystick events will be generated
        x = pygame.joystick.Joystick(n)
        x.init()
        joysticks.append(x)

def finalize():
    """
    Shut down joystick features.
    """
    pygame.joystick.quit()

def getJoystickFeatures():
    """
    Returns a list with one tuple for each joystick.  Each tuple
    specifies the number of axes, balls, buttons and hats for the
    joystick (in that order).
    """
    global joysticks
    r = []
    for js in joysticks:
        r.append((js.get_numaxes(), js.get_numballs(), js.get_numbuttons(), js.get_numhats()))
    return r

setJoystickCallbacks = eventpoll.setJoystickCallbacks
