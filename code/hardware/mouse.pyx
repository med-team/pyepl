# PyEPL: hardware/mouse.pyx
#
# Copyright (C) 2003-2005 Michael J. Kahana
# Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
# URL: http://memory.psych.upenn.edu/programming/pyepl
#
# Distributed under the terms of the GNU Lesser General Public License
# (LGPL). See the license.txt that came with this file.

"""
This module provides low-level mouse functionality.
"""

import pygame
import eventpoll
import timing

def initialize(**options):
    """
    Do any preparation necessary before using mouse features.
    """
    # moved call to setMouseVisibility to hardware.graphics.startVideo()
    pass

def finalize():
    """
    Shut down mouse features.
    """
    setMouseCallbacks(None, None)

setMousePosition = pygame.mouse.set_pos

setMouseVisibility = pygame.mouse.set_visible  # True corrupts position change, False corrupts absolute position

def getMouseRange():
    return pygame.display.get_surface().get_size()

setMouseCallbacks = eventpoll.setMouseCallbacks
