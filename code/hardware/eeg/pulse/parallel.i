%module parallel

%{
#include "parallel.h"
%}

#ifdef SWIGPYTHON
%typemap(in) unsigned char * {
$1 = (unsigned char *)PyString_AsString($input);
}
#endif

/* %typemap(python, in) unsigned char * { */
/* $1 = (unsigned char *)PyString_AsString($input); */
/* } */

%include exception.i
%exception {
try {
    $function
}
catch(ParallelException) {
SWIG_exception(SWIG_RuntimeError, "Uncaught exception from Parallel port interface!\nSee line beginning \"Parallel port error\" above for details.");
}
}

%include "parallel.h"
