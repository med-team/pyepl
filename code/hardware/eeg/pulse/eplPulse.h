// PyEPL: hardware/eeg/pulse/eplPulse.h
//
// Copyright (C) 2003-2005 Michael J. Kahana
// Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
// URL: http://memory.psych.upenn.edu/programming/pyepl
//
// Distributed under the terms of the GNU Lesser General Public License
// (LGPL). See the license.txt that came with this file.

#ifndef _EPLPULSE_H_
#define _EPLPULSE_H_


#include <linux/parport.h>
#include <linux/ppdev.h>
#include <fcntl.h>
#include <sys/ioctl.h>

int eplOpenPort();
void eplClosePort();
int eplSetPortState(int state);


#endif
