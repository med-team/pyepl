#ifndef PARALLEL_H
#define PARALLEL_H

#include <linux/parport.h>
#include <linux/ppdev.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <string>
#include <iostream>

#include <cstdlib>
#include <cstring>
#include <math.h>

using namespace std;
/* using std::string; */
/* using std::cerr; */
/* using std::endl; */

class Parallel {
  int fd;
  static const char *devstr;
  static const unsigned int numPins;
  unsigned char binaryStringToNum(unsigned char *);
 public:
  Parallel();
  ~Parallel();
  void setState(int);
  void setSignal(unsigned char *);
};

class ParallelException{
  string msg;
 public:
 ParallelException(string s):msg(s){}
  void debug_print(){ cerr << "Parallel port error: " << msg << endl; }
};

#endif
