// PyEPL: hardware/eeg/scalp/shmem_struct.h
//
// Copyright (C) 2003-2005 Michael J. Kahana
// Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
// URL: http://memory.psych.upenn.edu/programming/pyepl
//
// Distributed under the terms of the GNU Lesser General Public License
// (LGPL). See the license.txt that came with this file.

#ifndef _SHMEM_STRUCT_H_
#define _SHMEM_STRUCT_H_

#include "cardinfo.h"

                                                                             
#define DUR 100			/* Size of circular buffer in seconds        */
#define HIST SR/SR 		/* history buffer size in elements           */
#define SHARE_SEM 312        	/* The Key to create sempahore               */
#define SHARE_KEY 316        	/* The Key to create share memory            */
/*---------------------------------------------------------------------------*/

#define FILE_STOPPED   0
#define FILE_REC_START 1
#define FILE_RECORDING 2
#define FILE_REC_STOP  3

#define FILE_NAME_SIZE 1024

typedef struct shared_use_st 
{
	short int file_state;
	char file_name[FILE_NAME_SIZE];
	unsigned long  int  time;
	unsigned long  int  raw_pos;
  /*
    when C arrays are stored in row major order. ie. the first
    dimension varies most slowly, and the last dimension varies most
    quickly.  SO, we make CH the second dimension in this array so
    that we can easily memcpy data, as we get data from the card with
    channels contiguously.
  */
  unsigned short int  raw[DUR*SR][CH];
  int magic;
} shared_use_st;
/*----------------------------------------------------------------------*/


/*
to be removed after we test josh's row major order changes
#define RAW_OUTER CH 	        /* Width of raw circular buffer              
#define RAW_INNER DUR*SR	/* Length of raw circular buffer             
unsigned short int  raw[RAW_OUTER][RAW_INNER];
*/

#endif
