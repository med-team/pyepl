// PyEPL: hardware/eeg/scalp/epleeg_wrapper.cpp
//
// Copyright (C) 2003-2005 Michael J. Kahana
// Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
// URL: http://memory.psych.upenn.edu/programming/pyepl
//
// Distributed under the terms of the GNU Lesser General Public License
// (LGPL). See the license.txt that came with this file.

#include "epleeg.h"


// create instance of class
EEG eeg;


// Wrapped methods

extern "C" int eegAttached()
{
  return eeg.Attached();
}

extern "C" int eegShmAttach()
{
  return eeg.ShmAttach();
}

extern "C" int eegShmDetach()
{
  return eeg.ShmDetach();
}

extern "C" int eegRecStart(char *filename)
{
  return eeg.RecStart(filename);
}

extern "C" int eegRecStop()
{
  return eeg.RecStop();
}

extern "C" long eegGetOffset()
{
  return eeg.GetOffset();
}

