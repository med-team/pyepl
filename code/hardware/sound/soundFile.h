// PyEPL: hardware/sound/soundFile.h
//
// Copyright (C) 2003-2005 Michael J. Kahana
// Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
// URL: http://memory.psych.upenn.edu/programming/pyepl
//
// Distributed under the terms of the GNU Lesser General Public License
// (LGPL). See the license.txt that came with this file.
// soundFile class header
//

#ifndef __SOUNDFILE_H
#define __SOUNDFILE_H

#include <sndfile.h>
#include <samplerate.h>

// From stdio.h
#ifndef SEEK_SET
#define SEEK_SET        0       /* set file offset to offset */
#endif
#ifndef SEEK_CUR
#define SEEK_CUR        1       /* set file offset to current plus offset */
#endif
#ifndef SEEK_END
#define SEEK_END        2       /* set file offset to EOF plus offset */
#endif

#define SHORT_RANGE 32767

class soundFile
{
 public:
  soundFile(const char *filename, int mode=SFM_READ, int format=0, int channels=0, int samplerate=0);
  ~soundFile();
  long getTotalSamples();
  int getChannels();
  int getSamplerate();
  int getFormat();
  long getFrames();
  float *readfile_float(int resampledrate=0);
  short *readfile_short(int resampledrate=0);
  int append_float(float *data, long numSamples);  
  int append_short(short *data, long numSamples);
 private:
  int open(const char *filename, int mode=SFM_READ, int format=0, int channels=0,int samplerate=0);
  int close();
  float *resample(float *indata, double ratio);
  SF_INFO info;
  SNDFILE *file;
};


#endif
