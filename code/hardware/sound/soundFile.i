// PyEPL: hardware/sound/soundFile.i
//
// Copyright (C) 2003-2005 Michael J. Kahana
// Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
// URL: http://memory.psych.upenn.edu/programming/pyepl
//
// Distributed under the terms of the GNU Lesser General Public License
// (LGPL). See the license.txt that came with this file.

%module soundFile
 // Must be careful with this typemap because other float * will be converted, 
 // but this typemap requires that it only apply to the readfile method.

#ifdef SWIGPYTHON
%typemap(out) float * {
  // set the result, getting the numread from the class
  $result = PyString_FromStringAndSize((char *)$1,((arg1)->getTotalSamples())*sizeof(float));
  delete $1;
}
%typemap(out) short * {
  // set the result, getting the numread from the class
  $result = PyString_FromStringAndSize((char *)$1,((arg1)->getTotalSamples())*sizeof(short));
  delete $1;
}

%typemap(in) float * {
  char *buffer;
  buffer = PyString_AsString($input);
  $1 = (float *)buffer;
}

%typemap(in) short * {
  char *buffer;
  buffer = PyString_AsString($input);
  $1 = (short *)buffer;
}
#endif

%{
#include "soundFile.h"
%}

%include "soundFile.h"

%include "constants.h"
