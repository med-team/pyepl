// PyEPL: hardware/sound/eplSound.h
//
// Copyright (C) 2003-2005 Michael J. Kahana
// Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
// URL: http://memory.psych.upenn.edu/programming/pyepl
//
// Distributed under the terms of the GNU Lesser General Public License
// (LGPL). See the license.txt that came with this file.
/*
  - Allow for resampling on append to fifo.
  - Separate recstart and playstart
*/


#ifndef __EPLSOUND_H
#define __EPLSOUND_H

#include "RtAudio.h"
#include "MyType.h"
#include "fifo.h"

class audioBuffer
{
 public:
  audioBuffer(long recLen, long playLen, 
	      unsigned int inRecChans, unsigned int inPlayChans, 
	      unsigned int samprate);
  ~audioBuffer();
  fifo *recBuf;
  fifo *playBuf;
  int recChans;
  int playChans;
  int rate;

  int recording;
  long samplesPlayed;
};

// This should probably match what's in MyType.h
#define FORMAT RTAUDIO_SINT16

class eplSound 
{
 public:
  eplSound(long recLen=REC_BUF_LEN, long playLen=PLAY_BUF_LEN, 
	   unsigned int sampleRate=SAMPLE_RATE, 
	   unsigned int bufSize=BUF_SIZE);
  ~eplSound();
  long append(MY_TYPE *newdata, long length, int overwrite, float ampFactor);
  long consume(MY_TYPE *newdata, long length);
  void clear();
  void clearPlayBuffer();
  void clearRecBuffer();
  int recstart();
  int recstop();
  int startstream();
  int stopstream(int abort=0);
  int getBufferSize();
  long getSamplesPlayed();
  void resetSamplesPlayed();
  int getRecChans();
  int getPlayChans();
  int getSampleRate();
  unsigned int getPlayStreamSampleRate();
  unsigned int getRecStreamSampleRate();
  long getBufferUsed();
  long getPlayStreamLatency();
  long getRecStreamLatency();
  static const int SCALE=32767;  
  static const unsigned int SAMPLE_RATE=44100;
  static const int SAMPLE_SILENCE=0;
  static const unsigned int BUF_SIZE=256;
  static const int NUM_INTERNAL_BUFF=4;
  static const int DEVICE=0;
  static const int FORMAT_SIZE=2;
  static const long PLAY_BUF_LEN=60;
  static const long REC_BUF_LEN=60;
  static const int NUM_CHANNELS=2;
 private:
  //RtAudio audio;
  RtAudio *playAudio;
  RtAudio *recAudio;
  int isDuplex;
  unsigned int playChans;
  unsigned int recChans;
  //int rate;
  unsigned int bufferSize;
  unsigned int playDevice;
  unsigned int recDevice;
  unsigned int sampleRate;
  unsigned int streaming;
  audioBuffer *data;
};

// audio callback function
int inout( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
           double streamTime, RtAudioStreamStatus status, void *data );
//int inout(char *buffer, int buffer_size, void *data);
//int playcall(char *buffer, int buffer_size, void *data);
//int reccall(char *buffer, int buffer_size, void *data);

#endif
